import com.google.gson.Gson;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class Resender implements Runnable {

    private MyInfo myInfo;
    private HashMap<String, Message> unconfirmedMessages;
    private HashMap<String, Long> confirmationTable;
    public static final int CONFIRM_TIMEOUT = 3000;
    public static final int SEC_WAIT = 1;
    private DatagramSocket socket;

    public Resender(MyInfo myInfo, DatagramSocket socket, HashMap<String, Message> unconfirmedMessages, HashMap<String, Long> confirmationTable) {
        this.myInfo = myInfo;
        this.unconfirmedMessages = unconfirmedMessages;
        this.socket = socket;
        this.confirmationTable = confirmationTable;
    }

    public void run() {
        Gson gson = new Gson();
        try {
            while (true) {

                synchronized (unconfirmedMessages) {
                    Iterator it = unconfirmedMessages.entrySet().iterator();

                    while (it.hasNext()) {
                        Map.Entry pair = (Map.Entry) it.next();
                        String id = (String) pair.getKey();
                        Message ms = (Message) pair.getValue();
                        Message m;
                        if(ms.getType()==MessageType.CHAT)
                            m = (ChatMessage)ms;
                        else m = (Message)ms;

                        Long time;
                        synchronized (confirmationTable) {
                            time = confirmationTable.get(id);
                        }

                        if (System.currentTimeMillis() - time > CONFIRM_TIMEOUT) { //если со времени отправки прошло больше CONFIRM_TIMEOUT секунд то переотправить
                            String msg = gson.toJson(m);
                            String ip = m.getIP();
                            socket.send(new DatagramPacket(msg.getBytes(), msg.length(), InetAddress.getByName(ip), m.getPort())); //отправить повторно

                            synchronized (confirmationTable) {
                                confirmationTable.put(id, System.currentTimeMillis());
                            }
                        }
                    }
                }
                TimeUnit.SECONDS.sleep(SEC_WAIT);
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
