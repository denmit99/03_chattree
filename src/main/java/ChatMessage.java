public class ChatMessage extends Message {

    private String text;

    public ChatMessage (MessageType type, String id, String text, String name, String ip, int port){
        super(type, id, name, ip, port);
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
