import java.util.ArrayList;
import java.util.HashMap;

public class MyInfo {
    private String myIP;
    private int myPort;
    private String myName;
    private String myAlterIP;
    private int myAlterPort;
    private ArrayList<HostInfo> neighbors;
    private int loss;


    public MyInfo(String myIP, int myPort, String myName, String myAlterIP, int myAlterPort, ArrayList<HostInfo> neighbors, int loss) {
        this.myIP = myIP;
        this.myPort = myPort;
        this.myName = myName;
        this.myAlterIP = myAlterIP;
        this.myAlterPort = myAlterPort;
        this.neighbors = neighbors;
        this.loss = loss;
    }

    public String getMyIP() {
        return myIP;
    }

    public void setMyIP(String myIP) {
        this.myIP = myIP;
    }

    public int getMyPort() {
        return myPort;
    }

    public void setMyPort(int myPort) {
        this.myPort = myPort;
    }

    public String getMyName() {
        return myName;
    }

    public void setMyName(String myName) {
        this.myName = myName;
    }

    public String getMyAlterIP() {
        return myAlterIP;
    }

    public void setMyAlterIP(String myAlterIP) {
        this.myAlterIP = myAlterIP;
    }

    public int getMyAlterPort() {
        return myAlterPort;
    }

    public void setMyAlterPort(int myAlterPort) {
        this.myAlterPort = myAlterPort;
    }

    public ArrayList<HostInfo> getNeighbors() {
        return neighbors;
    }

    public void setNeighbors(ArrayList<HostInfo> neighbors) {
        this.neighbors = neighbors;
    }

    public int getLoss() {
        return loss;
    }

    public void setLoss(int loss) {
        this.loss = loss;
    }
}
