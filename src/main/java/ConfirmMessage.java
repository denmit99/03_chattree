public class ConfirmMessage extends Message{

    private String alterIP;
    private int alterPort;

    public ConfirmMessage(MessageType type, String id, String name, String ip, int port, String alterIP, int alterPort){
        super(type, id, name, ip, port);
        this.alterIP = alterIP;
        this.alterPort = alterPort;
    }

    public String getAlterIP() {
        return alterIP;
    }

    public void setAlterIP(String alterIP) {
        this.alterIP = alterIP;
    }

    public int getAlterPort() {
        return alterPort;
    }

    public void setAlterPort(int alterPort) {
        this.alterPort = alterPort;
    }
}
