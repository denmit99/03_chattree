import com.google.gson.Gson;

import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class Main {

    private static final String myIP = "127.0.0.1";
    private static int myPort = 3372;
    private static String myName = "Sue";
    private static int loss = 30;
    private static int neighborPort = 3371;
    private static String neighborIP = "127.0.0.1";
    private static ArrayList<HostInfo> neighbors = new ArrayList<HostInfo>(); //список соседей
    private static HashMap<String, Message> unconfirmedMessages = new HashMap<String, Message>(); //неподтверженные сообщения
    private static HashMap<String, Long> confirmationTable = new HashMap<String, Long>(); //время начала ожидания подтверждения
    private static HashMap<HostInfo, Long> aliveTable = new HashMap<HostInfo, Long>(); //таблица времени последних признаков жизни узла
    private static String myAlterIP = null; //IP заместителя узла
    private static int myAlterPort = 0; //Порт заместителя узла

    public static void main(String[] args) {
        try {
            if ((loss > 100) || (loss < 0))
                loss = 50;

            MyInfo myInfo = new MyInfo(myIP, myPort, myName, myAlterIP, myAlterPort, neighbors, loss);
            System.out.println("[" + myIP + ":" + myPort + "] Узел с именем \"" + myName + "\", потери " + loss + "%");
            DatagramSocket socket = new DatagramSocket(myPort, InetAddress.getByName(myIP));
            Thread aliver = new Thread(new Aliver(myInfo, socket, aliveTable));
            Thread chatSender = new Thread(new ChatSender(myInfo, socket, unconfirmedMessages, confirmationTable));
            Thread chatReceiver = new Thread(new ChatReceiver(myInfo, socket, unconfirmedMessages, confirmationTable, aliveTable));
            Thread resender = new Thread(new Resender(myInfo, socket, unconfirmedMessages, confirmationTable));

            resender.start();
            chatReceiver.start();
            chatSender.start();
            aliver.start();

            if (neighborPort != 0) { //потом заменить просто на отсутствие аргумента
                neighbors.add(new HostInfo(neighborIP, neighborPort));
                String id = UUID.randomUUID().toString();
                Message connectMessage = new Message(MessageType.CONNECT, id, myName, neighborIP, neighborPort);

                myInfo.setMyAlterIP(neighborIP); //назначаем заместителем того, к кому присоединяемся
                myInfo.setMyAlterPort(neighborPort);
                sendMessageToSocket(connectMessage, socket);
                System.out.println("send CONNECT " + "[" + id + "]" + " to " + neighborIP + ":" + neighborPort);
                unconfirmedMessages.put(id, connectMessage);
                System.out.println("this CONNECT " + "[" + id + "]" + "added to unconfirmed");
                confirmationTable.put(id, System.currentTimeMillis());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void sendMessageToSocket(Message message, DatagramSocket socket) {
        Gson gson = new Gson();
        String msg = gson.toJson(message); //это как-то покрасивее сделать чтобы просто делать send Message
        try {
            socket.send(new DatagramPacket(msg.getBytes(), msg.length(), InetAddress.getByName(message.getIP()), message.getPort())); //оповестить соседа о присоединении
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
