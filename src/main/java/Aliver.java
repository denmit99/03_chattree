import com.google.gson.Gson;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class Aliver implements Runnable {

    private MyInfo myInfo;
    private DatagramSocket socket;
    private ArrayList<HostInfo> neighbors;
    private HashMap<HostInfo, Long> aliveTable;
    private static final int ALIVE_TIME = 10000;
    private static final int DELAY = 3;

    public Aliver(MyInfo myInfo, DatagramSocket socket, HashMap<HostInfo, Long> aliveTable) {
        this.myInfo = myInfo;
        this.socket = socket;
        neighbors = myInfo.getNeighbors();
        this.aliveTable = aliveTable;
    }

    public void run() {

        Gson gson = new Gson();
        try {
            while (true) {
                for (HostInfo host : neighbors) { //отправить всем соседям
                    String id = UUID.randomUUID().toString();
                    Message aliveMessage = new Message(MessageType.ALIVE, id, myInfo.getMyName(), host.getIpAddress(), host.getPort());
                    String msg = gson.toJson(aliveMessage);
                    socket.send(new DatagramPacket(msg.getBytes(), msg.length(), InetAddress.getByName(host.getIpAddress()), host.getPort()));
                }

                synchronized (aliveTable) {
                    Iterator it = aliveTable.entrySet().iterator();

                    while (it.hasNext()) {
                        Map.Entry pair = (Map.Entry) it.next();
                        HostInfo host = (HostInfo) pair.getKey();
                        Long time = (Long) pair.getValue();
                        if (System.currentTimeMillis() - time > ALIVE_TIME) { //если от хоста давно не было признаков жизни
                            for (HostInfo h : neighbors) {
                                if (h.equalsTo(host)) {
                                    String newNeighborIP = h.getAlterIP();
                                    int newNeighborPort = h.getAlterPort();
                                    if (!(newNeighborIP.equals(myInfo.getMyIP())) || (newNeighborPort != myInfo.getMyPort())) { //чтобы не добавить себя в своих соседей
                                        neighbors.add(new HostInfo(newNeighborIP, newNeighborPort)); //добавить в соседи его заместителя
                                        Message connectMessage = new Message(MessageType.CONNECT, UUID.randomUUID().toString(), myInfo.getMyName(), newNeighborIP, newNeighborPort);
                                        myInfo.setMyAlterIP(newNeighborIP); //назначаем заместителем того, к кому присоединяемся
                                        myInfo.setMyAlterPort(newNeighborPort);
                                        Main.sendMessageToSocket(connectMessage, socket);
                                    }
                                    synchronized (neighbors) {
                                        neighbors.remove(h);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
                TimeUnit.SECONDS.sleep(DELAY);
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}