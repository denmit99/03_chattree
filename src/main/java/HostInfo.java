public class HostInfo  {
    private String ipAddress;
    private int port;
    private String alterIP;
    private int alterPort;

    public HostInfo(String ipAddress, int port) {
        this.ipAddress = ipAddress;
        this.port = port;
        alterIP = null;
        alterPort = 0;
    }

    @Override
    public int hashCode() {
        return ipAddress.hashCode()+port;
    }

    @Override
    public boolean equals(Object obj) {
        return ((this.ipAddress.equals(((HostInfo)obj).ipAddress))&&(port==((HostInfo)obj).port));
    }

    public int getPort() {
        return port;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public String getAlterIP() {
        return alterIP;
    }

    public void setAlterIP(String alterIP) {
        this.alterIP = alterIP;
    }

    public int getAlterPort() {
        return alterPort;
    }

    public void setAlterPort(int alterPort) {
        this.alterPort = alterPort;
    }

    public boolean equalsTo(HostInfo h) {
        return ((this.ipAddress.equals(h.ipAddress)) && (this.port == h.port));
    }

}
