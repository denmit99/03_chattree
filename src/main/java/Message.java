public class Message {
    private MessageType type;
    private String messageID;
    private String name;
    private String ip;
    private int port;

    public Message(MessageType type, String messageID, String name, String ip, int port) {
        this.type = type;
        this.messageID = messageID;
        this.name = name;
        this.ip = ip;
        this.port = port;
    }

    public MessageType getType() {
        return type;
    }

    public String getMessageID() {
        return messageID;
    }

    public String getIP() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    public String getName() {
        return name;
    }
}
