import com.google.gson.Gson;

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.*;

public class ChatReceiver implements Runnable {

    private static final int BUFSIZE = 1024;
    private static final int RM_BUF_SIZE = 1000;
    private DatagramSocket socket;
    private ArrayList<HostInfo> neighbors;
    private int loss;
    private ArrayList<String> receivedMessages = new ArrayList<String>();
    private HashMap<String, Message> unconfirmedMessages; //неподтвержденные сообщения
    private HashMap<String, Long> confirmationTable;
    private HashMap<HostInfo, Long> aliveTable;
    private String myName;
    private MyInfo myInfo;

    public ChatReceiver(MyInfo myInfo, DatagramSocket socket, HashMap<String, Message> unconfirmedMessages, HashMap<String, Long> confirmationTable, HashMap<HostInfo, Long> aliveTable) {
        this.socket = socket;
        this.neighbors = myInfo.getNeighbors();
        this.loss = myInfo.getLoss();
        this.unconfirmedMessages = unconfirmedMessages;
        this.confirmationTable = confirmationTable;
        myName = myInfo.getMyName();
        this.myInfo = myInfo;
        this.aliveTable = aliveTable;
    }

    public void run() {
        try {
            Gson gson = new Gson();
            while (true) {
                DatagramPacket packet = new DatagramPacket(new byte[BUFSIZE], BUFSIZE);
                socket.receive(packet); //получили пакет
                System.out.println("pocket received");
                if (!pocketLost()) {
                    HostInfo host = new HostInfo(packet.getAddress().getHostAddress(), packet.getPort()); //узнали инфу об отправителе
                    String jsonString = new String(packet.getData(), 0, packet.getLength());
                    synchronized (aliveTable) {
                        aliveTable.put(host, System.currentTimeMillis());
                    }

                    switch (gson.fromJson(jsonString, Message.class).getType()) { //в зависимости от типа сообщения
                        case CHAT: {
                            ChatMessage chatMessage = gson.fromJson(jsonString, ChatMessage.class);
                            String text = chatMessage.getText();
                            String id = chatMessage.getMessageID();
                            String authorName = chatMessage.getName();

                            if (!alreadyReceived(id)) {
                                System.out.println("[" + authorName + "]: " + text);
                                receivedMessages.add(id);

                                String confirmMessage = gson.toJson(new ConfirmMessage(MessageType.CONFIRM, id, myName, host.getIpAddress(), host.getPort(), myInfo.getMyAlterIP(), myInfo.getMyAlterPort())); //в CONFIRM_MESSAGE хранится id сообщения которое подтверждается
                                socket.send(new DatagramPacket(confirmMessage.getBytes(), confirmMessage.length(), InetAddress.getByName(host.getIpAddress()), host.getPort()));
                                System.out.println("send CONFIRM \"" + text + "\"" + "[" + id + "]" + " to " + host.getIpAddress() + ":" + host.getPort());

                                if (!neighbors.isEmpty()) {
                                    for (HostInfo n : neighbors) { //отправить всем кроме того от которого пришло
                                        InetAddress ipToSend = InetAddress.getByName(n.getIpAddress());

                                        int portToSend = n.getPort();
                                        String newID = UUID.randomUUID().toString();
                                        ChatMessage newMessage = new ChatMessage(MessageType.CHAT, newID, text, authorName, n.getIpAddress(), portToSend);

                                        if ((!ipToSend.equals(packet.getAddress())) || (portToSend != packet.getPort())) { //если тот, от кого пришло, не отправлять
                                            String msg = gson.toJson(newMessage);
                                            socket.send(new DatagramPacket(msg.getBytes(), msg.length(), ipToSend, portToSend));
                                            System.out.println("send \"" + text + "\"" + "[" + id + "]" + " to neighbor " + ipToSend + ":" + portToSend);

                                            synchronized (unconfirmedMessages) {
                                                synchronized (confirmationTable) {
                                                    unconfirmedMessages.put(newID, newMessage);
                                                    confirmationTable.put(newID, System.currentTimeMillis());
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                        }
                        case CONNECT: {
                            if (!neighbors.contains(host)) { //если такого соседа нет, то добавить
                                host.setAlterIP(myInfo.getMyIP()); //заместителем у того, кто к нам присоединился очевидно являемся мы сами
                                host.setAlterPort(myInfo.getMyPort());
                                if ((myInfo.getMyAlterIP() == null) && (myInfo.getMyAlterPort() == 0)) { //если у нас нет заместителя, то он им и будет
                                    myInfo.setMyAlterIP(host.getIpAddress());
                                    myInfo.setMyAlterPort(host.getPort());
                                }
                                neighbors.add(host);
                                String confirmMessage = gson.toJson(new ConfirmMessage(MessageType.CONFIRM, UUID.randomUUID().toString(), myName, host.getIpAddress(), host.getPort(), myInfo.getMyAlterIP(), myInfo.getMyAlterPort()));
                                socket.send(new DatagramPacket(confirmMessage.getBytes(), confirmMessage.length(), InetAddress.getByName(host.getIpAddress()), host.getPort()));
                                System.out.println("send CONFIRM on connect to " + host.getIpAddress() + ":" + host.getPort());

                                continue;
                            }
                            break;
                        }
                        case CONFIRM: {
                            ConfirmMessage confirmMessage = gson.fromJson(jsonString, ConfirmMessage.class);
                            for (HostInfo h : neighbors) {//добавить заместителя нашего соседа
                                if ((h.getIpAddress().equals(host.getIpAddress())) && (h.getPort() == host.getPort())) {
                                    h.setAlterIP(confirmMessage.getAlterIP());
                                    h.setAlterPort(confirmMessage.getAlterPort());
                                }
                            }
                            synchronized (unconfirmedMessages) {
                                System.out.println("[" + confirmMessage.getMessageID() + "] confirmed and removed");
                                unconfirmedMessages.remove(confirmMessage.getMessageID());
                            }
                            break;
                        }
                        case ALIVE: {
                            break;
                        }
                    }
                }
                else{
                    System.out.println("pocket lost");
                }
                if (receivedMessages.size() > RM_BUF_SIZE) {
                    receivedMessages.clear();
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    boolean pocketLost() {
        return ((int) (Math.random() * 100) < loss);
    }

    boolean alreadyReceived(String id) {
        return (receivedMessages.contains(id));
    }
}
