import com.google.gson.Gson;

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class ChatSender implements Runnable {

    private ArrayList<HostInfo> neighbors; //узлы-соседи
    private HashMap<String, Message> unconfirmedMessages; //неподтвержденные сообщения
    private HashMap<String, Long> confirmationTable;
    private String myName;
    private MyInfo myInfo;

    private DatagramSocket socket;

    public ChatSender(MyInfo myInfo, DatagramSocket socket, HashMap<String, Message> unconfirmedMessages, HashMap<String, Long> confirmationTable) {
        this.neighbors = myInfo.getNeighbors();
        this.socket = socket;
        this.unconfirmedMessages = unconfirmedMessages;
        this.confirmationTable = confirmationTable;
        myName = myInfo.getMyName();
    }

    public void run() {
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            Gson gson = new Gson();

            while (true) {
                if (in.ready()) { //если есть что прочитать
                    String text = in.readLine(); //прочитать
                    for (HostInfo host : neighbors) { //отправить всем соседям
                        String id = UUID.randomUUID().toString();
                        ChatMessage chatMessage = new ChatMessage(MessageType.CHAT, id, text, myName, host.getIpAddress(), host.getPort());
                        String msg = gson.toJson(chatMessage);
                        socket.send(new DatagramPacket(msg.getBytes(), msg.length(), InetAddress.getByName(host.getIpAddress()), host.getPort()));
                        System.out.println("send \"" + text + "\"" + "[" + id + "]" + " to " + host.getIpAddress() + ":" + host.getPort());

                        synchronized (unconfirmedMessages) {
                            synchronized (confirmationTable) {
                                unconfirmedMessages.put(id, chatMessage);
                                System.out.println("\"" + text + "\"" + "[" + id + "]" + "added to unconfirmed");
                                confirmationTable.put(id, System.currentTimeMillis());
                            }
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
