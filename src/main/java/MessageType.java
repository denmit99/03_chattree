public enum MessageType {
    CHAT,
    CONFIRM,
    CONNECT,
    ALIVE
}
